import unittest
from budget_sync.test.mock_bug import MockBug
from budget_sync.util import BugStatus


class TestBugStatus(unittest.TestCase):
    def test_values(self):
        for i in BugStatus:
            self.assertIs(i, getattr(BugStatus, i.value))
            self.assertEqual(str(i), i.value)
            self.assertEqual(repr(i), f"BugStatus.{i.value}")

    def test_cast(self):
        for i in BugStatus:
            self.assertEqual(i, BugStatus.cast(i))
            self.assertEqual(i, BugStatus.cast(str(i)))
            self.assertEqual(i, BugStatus.cast(str(i), unknown_allowed=True))
        with self.assertRaises(ValueError):
            BugStatus.cast("<unknown>")
        self.assertEqual("<unknown>",
                         BugStatus.cast("<unknown>", unknown_allowed=True))


class TestMockBug(unittest.TestCase):
    maxDiff = None

    def test_repr(self):
        bug = MockBug(bug_id=12)
        self.assertEqual(
            repr(bug),
            "MockBug(bug_id=12, cf_budget_parent=None, cf_budget='0', "
            "cf_total_budget='0', cf_nlnet_milestone='---', "
            "cf_payees_list='', summary='<default summary>', "
            "status=BugStatus.CONFIRMED, "
            "assigned_to='user@example.com')")
        bug = MockBug(bug_id=34,
                      cf_budget_parent=1,
                      cf_budget="45",
                      cf_total_budget="23",
                      cf_nlnet_milestone="abc",
                      cf_payees_list="# a",
                      summary="blah blah",
                      status="blah",
                      assigned_to="fake-email@example.com")
        self.assertEqual(
            repr(bug),
            "MockBug(bug_id=34, cf_budget_parent=1, cf_budget='45', "
            "cf_total_budget='23', cf_nlnet_milestone='abc', "
            "cf_payees_list='# a', summary='blah blah', status='blah', "
            "assigned_to='fake-email@example.com')")

    def test_cf_budget_parent(self):
        bug = MockBug(bug_id=1, cf_budget_parent=None)
        with self.assertRaises(AttributeError):
            bug.cf_budget_parent
        self.assertIsNone(getattr(bug, "cf_budget_parent", None))
        bug.cf_budget_parent = 1
        self.assertEqual(bug.cf_budget_parent, 1)
        with self.assertRaises(TypeError):
            bug.cf_budget_parent = "abc"
        del bug.cf_budget_parent
        with self.assertRaises(AttributeError):
            bug.cf_budget_parent
        with self.assertRaises(AttributeError):
            del bug.cf_budget_parent
        with self.assertRaises(AttributeError):
            bug.cf_budget_parent
        bug.cf_budget_parent = 5
        self.assertEqual(bug.cf_budget_parent, 5)
        bug = MockBug(bug_id=1, cf_budget_parent=2)
        self.assertEqual(bug.cf_budget_parent, 2)


if __name__ == "__main__":
    unittest.main()
