import os
import re
import sys
import json
from typing import Optional
from budget_sync.ordered_set import OrderedSet
from budget_sync.write_budget_csv import write_budget_csv
from bugzilla import Bugzilla
import logging
import argparse
from pathlib import Path
from budget_sync.util import all_bugs, tty_out
from budget_sync.config import Config, ConfigParseError
from budget_sync.budget_graph import BudgetGraph, PaymentSummary
from budget_sync.write_budget_markdown import (write_budget_markdown,
                                               markdown_for_person)

def spc(s):
    return s # not needed when using <pre> instead of <tt>
    return s.replace(" ", "&nbsp;")

bugurl = "https://bugs.libre-soc.org/show_bug.cgi?id="

def main():
    parser = argparse.ArgumentParser(
        description="Check for errors in "
        "Libre-SOC's style of budget tracking in Bugzilla.")
    parser.add_argument(
        "-c", "--config", type=argparse.FileType('r'),
        required=True, help="The path to the configuration TOML file",
        dest="config", metavar="<path/to/budget-sync-config.toml>")
    parser.add_argument(
        "-o", "--output-dir", type=Path, default=None,
        help="The path to the output directory, will be created if it "
        "doesn't exist",
        dest="output_dir", metavar="<path/to/output/dir>")
    parser.add_argument('--subset',
                        help="write the output for this subset of bugs",
                        metavar="<bug-id>,<bug-id>,...")
    parser.add_argument('--subset-person',
                        help="write the output for this person",
                        dest="person")
    parser.add_argument('--username',
                        help="Log in with this Bugzilla username")
    parser.add_argument('--password',
                        help="Log in with this Bugzilla password")
    parser.add_argument('--comments', action='store_true',
                        help="Put JSON into comments")
    parser.add_argument('--detail', action='store_true',
                        help="Add detail to report (links description people)")
    args = parser.parse_args()
    try:
        with args.config as config_file:
            config = Config.from_file(config_file)
    except (IOError, ConfigParseError) as e:
        logging.error("Failed to parse config file: %s", e)
        return
    logging.info("Using Bugzilla instance at %s", config.bugzilla_url)
    # download all bugs and create a summary report
    reportdir = "./" if args.output_dir is None else str(args.output_dir)
    reportdir = "/".join(reportdir.split("/")[:-1]) # strip /mdwn
    reportname = reportdir + "/report.mdwn"
    with open(reportname, "w") as f:
        print("<pre>", file=f)  # for using the output as markdown
        bz = Bugzilla(config.bugzilla_url)
        if args.username:
            logging.debug("logging in...")
            bz.interactive_login(args.username, args.password)
        logging.debug("Connected to Bugzilla")
        budget_graph = BudgetGraph(all_bugs(bz), config)
        for error in budget_graph.get_errors():
            logging.error("%s", error)
        if args.person or args.subset:
            if not args.person:
                logging.fatal("must use --subset-person with --subset option")
                sys.exit(1)
            print_markdown_for_person(f, budget_graph, config,
                                      args.person, args.subset)
            return
        if args.output_dir is not None:
            write_budget_markdown(budget_graph, args.output_dir)
            write_budget_csv(budget_graph, args.output_dir)
        summarize_milestones(f, budget_graph, detail=args.detail)
        print("</pre>", file=f)  # for using the output as markdown
    # now create the JSON milestone files for putting into NLnet RFP system
    json_milestones(budget_graph, args.comments, args.output_dir)


def print_markdown_for_person(f, budget_graph, config, person_str, subset_str):
    person = config.all_names.get(person_str)
    if person is None:
        logging.fatal("--subset-person: unknown person: %s", person_str)
        sys.exit(1)
    nodes_subset = None
    if subset_str:
        nodes_subset = OrderedSet()
        for bug_id in re.split(r"[\s,]+", subset_str):
            try:
                node = budget_graph.nodes[int(bug_id)]
            except (ValueError, KeyError):
                logging.fatal("--subset: unknown bug: %s", bug_id)
                sys.exit(1)
            nodes_subset.add(node)
    print(markdown_for_person(budget_graph, person, nodes_subset), file=f)


def print_budget_then_children(f, indent, nodes, bug_id, detail=False):
    """recursive indented printout of budgets
    """

    bug = nodes[bug_id]
    b_incl = str(bug.fixed_budget_including_subtasks)
    b_excl = str(bug.fixed_budget_excluding_subtasks)
    s_incl = str(bug.submitted_including_subtasks)
    p_incl = str(bug.paid_including_subtasks)
    if b_incl == s_incl and b_incl == p_incl:
        descr = "(s+p)"
    elif b_incl == s_incl:
        descr = "(s) p %s" % p_incl
    elif b_incl == p_incl:
        descr = "(p) s %s" % s_incl
    elif s_incl == p_incl:
        descr = " s,p  %s" % (p_incl)
    else:
        descr = "s %s p %s" % (s_incl, p_incl)
    excl_desc = "               "
    if b_incl != b_excl:
        excl_desc = "excltasks %6s" % b_excl
    bugid = spc("%5d" % bug.bug.id)
    buglink = "<a href='%s%d'>%s</a>" % (bugurl, bug.bug.id, bugid)
    print(spc("bug #URL %s budget %6s %s %s" %
              (' |   ' * indent,
               b_incl,
               excl_desc,
               descr
               )).replace("URL", buglink), file=f)
    if detail:
        print(spc("           %s |        (%s)" %
                  (' |   ' * indent,
                   bug.bug.summary[:40]
                  )), file=f)
    # print(repr(bug))

    for child in bug.immediate_children:
        if (str(child.budget_including_subtasks) == "0" and
                str(child.budget_excluding_subtasks) == "0"):
            continue
        print_budget_then_children(f, indent+1, nodes, child.bug.id, detail)


def summarize_milestones(f, budget_graph, detail=False):
    for milestone, payments in budget_graph.milestone_payments.items():
        summary = PaymentSummary(payments)
        print(f"{milestone.identifier}", file=f)
        print(f"    {summary.total} submitted: "
              f"{summary.total_submitted} paid: {summary.total_paid}",
              file=f)
        not_submitted = summary.get_not_submitted()
        if not_submitted:
            print("not submitted %s" % not_submitted, file=f)

        # and one to display people
        for person in budget_graph.milestone_people[milestone]:
            ident = "<a href='../mdwn/%s/'>%s</a>"
            ident %= (person.identifier, person.identifier)
            print(spc("    %-30s - %s" % (ident, person.full_name)), file=f)
        print("", file=f)

    # now do trees
    for milestone, payments in budget_graph.milestone_payments.items():
        print("%s %d" % (milestone.identifier, milestone.canonical_bug_id),
              file=f)
        print_budget_then_children(f, 0, budget_graph.nodes,
                                   milestone.canonical_bug_id, detail)
        print("", file=f)


def json_milestones(budget_graph: BudgetGraph, add_comments: bool,
                    output_dir: Path):
    """reports milestones as json format
    """
    bug_comments_map = {}
    if add_comments:
        need_set = set()
        bugzilla = None
        for nodes in budget_graph.assigned_nodes_for_milestones.values():
            for node in nodes:
                need_set.add(node.bug.id)
                bugzilla = node.bug.bugzilla
        need_list = sorted(need_set)
        total = len(need_list)
        with tty_out() as term:
            step = 100
            i = 0
            while i < total:
                cur_need = need_list[i:i + step]
                stop = i + len(cur_need)
                print("loading comments %d:%d of %d" % (i, stop, total),
                      flush=True, file=term)
                comments = bugzilla.get_comments(cur_need)['bugs']
                if len(comments) < len(cur_need) and len(cur_need) > 1:
                    step = max(1, step // 2)
                    print("failed, trying smaller step of %d" % step,
                          flush=True, file=term)
                    continue
                bug_comments_map.update(comments)
                i += len(cur_need)
    for milestone, payments in budget_graph.milestone_payments.items():
        summary = PaymentSummary(payments)
        # and one to display people
        ppl = []
        for person in budget_graph.milestone_people[milestone]:
            p = {'name': person.full_name, 'email': person.email}
            ppl.append(p)

        tasks = []
        canonical = budget_graph.nodes[milestone.canonical_bug_id]
        for child in canonical.immediate_children:
            milestones = []
            # include the task itself as a milestone
            for st in list(child.children()) + [child]:
                amount = st.fixed_budget_excluding_subtasks.int()
                if amount == 0:  # skip anything at zero
                    continue
                # if "task itself" then put the milestone as "wrapup"
                if st.bug == child.bug:
                    description = 'wrapup'
                    intro = []
                else:
                    # otherwise create a description and get comment #0
                    description = "%d %s" % (st.bug.id, st.bug.summary)
                    # add parent and MoU top-level
                    parent_id = st.parent.bug.id
                    if parent_id != child.bug.id:
                        description += "\n(Sub-sub-task of %d)" % parent_id
                task = {'description': description,
                        'amount': amount,
                        }
                #mou_bug = st.closest_bug_in_mou
                # if mou_bug is not None:
                #    task['mou_task'] = mou_bug.bug.id
                milestones.append(task)
            # create MoU task: get comment #0
            intro = []
            comment = "%s\n " % child.bug_url
            if add_comments:
                comment += "\n"
                comments = bug_comments_map[str(child.bug.id)]['comments']
                lines = comments[0]['text'].splitlines()
                for i, line in enumerate(lines):
                    # look for a line with only 2 or more `-` as the
                    # standard way in markdown of having a "break" (<hl />)
                    # this truncates the comment so that the RFP database
                    # has only the "summary description" but the rest may
                    # be used for "TODO" lists
                    l = line.strip()
                    if len(l) >= 2 and l == "-" * len(l):
                        lines[i:] = []
                        break
                comment += "\n".join(lines)
            intro.append(comment)
            # print (description, intro)
            # sys.stdout.flush()
            task = {'title': "%d %s" % (child.bug.id, child.bug.summary),
                    'intro': intro,
                    'amount': child.fixed_budget_including_subtasks.int(),
                    'url': "{{ %s }} " % child.bug_url,
                    'milestones': milestones
                    }
            tasks.append(task)

        d = {'participants': ppl,
             'preamble': '',
             'type': 'Group',
             'url': canonical.bug_url,
             'plan': {'intro': [''],
                      'tasks': tasks,
                      'rfp_secret': '',
                      }
             }

        output_file = output_dir / f"report.{milestone.identifier}.json"
        output_file.write_text(json.dumps(d, indent=2), encoding="utf-8")


if __name__ == "__main__":
    main()
