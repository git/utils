from typing import Any, Dict, Iterable, Iterator, MutableSet, Optional, TypeVar

__all__ = ['OrderedSet']
_T_co = TypeVar('_T_co')


class OrderedSet(MutableSet[_T_co]):
    __map: Dict[_T_co, None]

    def __init__(self, iterable: Iterable[_T_co] = ()):
        self.__map = {i: None for i in iterable}

    def __len__(self) -> int:
        return len(self.__map)

    def __contains__(self, key: Any) -> bool:
        return key in self.__map

    def add(self, key: _T_co):
        self.__map[key] = None

    def discard(self, key: Any):
        self.__map.pop(key, None)

    def __iter__(self) -> Iterator[_T_co]:
        return iter(self.__map.keys())

    def __repr__(self) -> str:
        if len(self) == 0:
            return "OrderedSet()"
        return f"OrderedSet({list(self)!r})"
