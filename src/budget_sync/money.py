LOG10_CENTS_PER_EURO = 2
CENTS_PER_EURO = 10 ** LOG10_CENTS_PER_EURO

__all__ = ["Money", "LOG10_CENTS_PER_EURO", "CENTS_PER_EURO"]


def _is_ascii_digits(s: str):
    try:
        s.encode("ascii")
    except UnicodeEncodeError:
        return False
    return s.isdigit()


class Money:
    """class for handling money, stored as an integer number of cents.

    Note: float is not appropriate for dealing with monetary values due to
    loss of precision and round-off error. Decimal has similar issues, but to
    a smaller extent."""

    cents: int

    __slots__ = ["cents"]

    def __init__(self, value=None, *, cents=None):
        if value is None:
            if cents is None:
                cents = 0
            assert isinstance(cents, int)
        elif isinstance(value, Money):
            cents = value.cents
        elif isinstance(value, int):
            cents = value * CENTS_PER_EURO
        elif isinstance(value, float):
            raise TypeError("float is not an appropriate type"
                            " for dealing with money")
        else:
            cents = self.from_str(value).cents
        self.cents = cents

    @staticmethod
    def from_str(text: str):
        if not isinstance(text, str):
            raise TypeError("Can't use Money.from_str to "
                            "convert from non-str value")
        parts = text.strip().split(".", maxsplit=2)
        first_part = parts[0]
        negative = first_part.startswith("-")
        if first_part.startswith(("-", "+")):
            first_part = first_part[1:]
        if first_part == "":
            euros = 0
        elif _is_ascii_digits(first_part):
            euros = int(first_part)
        else:
            raise ValueError("invalid Money string: characters after sign and"
                             " before first `.` must be ascii digits")
        if len(parts) > 2:
            raise ValueError("invalid Money string: too many `.` characters")
        elif len(parts) == 2:
            if parts[1] == "":
                if first_part == "":
                    raise ValueError("invalid Money string: missing digits")
                cents = 0
            elif _is_ascii_digits(parts[1]):
                shift_amount = LOG10_CENTS_PER_EURO - len(parts[1])
                if shift_amount < 0:
                    raise ValueError("invalid Money string: too many digits"
                                     " after `.`")
                cents = int(parts[1]) * (10 ** shift_amount)
            else:
                raise ValueError("invalid Money string: characters"
                                 " after `.` must be ascii digits")
        elif first_part == "":
            raise ValueError("invalid Money string: missing digits")
        else:
            cents = 0
        cents += CENTS_PER_EURO * euros
        if negative:
            cents = -cents
        return Money(cents=cents)

    def int(self):
        return int(str(self))

    def __str__(self):
        retval = "-" if self.cents < 0 else ""
        retval += str(abs(self.cents) // CENTS_PER_EURO)
        cents = abs(self.cents) % CENTS_PER_EURO
        if cents != 0:
            retval += "."
            retval += str(cents).zfill(LOG10_CENTS_PER_EURO)
        return retval

    def __lt__(self, other):
        return self.cents < Money(other).cents

    def __le__(self, other):
        return self.cents <= Money(other).cents

    def __eq__(self, other):
        return self.cents == Money(other).cents

    def __ne__(self, other):
        return self.cents != Money(other).cents

    def __gt__(self, other):
        return self.cents > Money(other).cents

    def __ge__(self, other):
        return self.cents >= Money(other).cents

    def __repr__(self):
        return f"Money({repr(str(self))})"

    def __bool__(self):
        return bool(self.cents)

    def __add__(self, other):
        cents = self.cents + Money(other).cents
        return Money(cents=cents)

    def __radd__(self, other):
        cents = Money(other).cents + self.cents
        return Money(cents=cents)

    def __sub__(self, other):
        cents = self.cents - Money(other).cents
        return Money(cents=cents)

    def __rsub__(self, other):
        cents = Money(other).cents - self.cents
        return Money(cents=cents)

    def __mul__(self, other):
        if not isinstance(other, int):
            raise TypeError("can't multiply by non-int")
        cents = self.cents * other
        return Money(cents=cents)

    def __rmul__(self, other):
        if not isinstance(other, int):
            raise TypeError("can't multiply by non-int")
        cents = other * self.cents
        return Money(cents=cents)
