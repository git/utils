from setuptools import setup, find_packages

install_requires = [
    "python-bugzilla",
    "toml",
    "cached-property",
]

setup(
    name="libre-soc-utils",
    version="0.0.1",
    author="Jacob Lifshay",
    author_email="programmerjake@gmail.com",
    url="https://git.libre-soc.org/?p=util",
    license="LGPL-2.1-or-later",
    packages=find_packages("src"),
    package_dir={"": "src"},
    zip_safe=False,
    entry_points={
        "console_scripts": [
            "budget-sync=budget_sync.main:main",
        ],
    },
    install_requires=install_requires,
)
